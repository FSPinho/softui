# SoftUI
Softness is the concept


# Usage example

Int the root div of your Angular 2 project, put:
```html
<landing-frame
  [title]="'SoftUI'"
  [logo]="'assets/img/favicon.ico'"
  [menuLinks]="[['Home', '#', 'home'], ['About Us', '#', 'info'], ['Contact', '#', 'phone']]"
  [activeLink]="'Home'"
  [actions]="[['Log In', '#'], ['Sign Up', '#']]"
  [footer]="{ text: 'Soft is the concept' }"
  [copyright]="{ year: 2017, text: 'SoftUI', privacyLink: '#' }">
  
  <landing-header class="header"
    [pages]="[
      {
        image: 'http://lorempixel.com/800/400/food/1', 
        title: 'Header title', 
        text: 'Header text', 
        actions: [['Begin', '#']] 
      },
      {
        image: 'http://lorempixel.com/800/400/food/2', 
        title: 'Page 02', 
        text: 'Page text', 
        actions: [['Begin', '#']] }
    ]">
    </landing-header>

  <div class="body">
    <landing-section [color]="blue">Section 1</landing-section>
    <landing-section [color]="orange">Section 1</landing-section>  
  </div>

</landing-frame>
```

# To do

Component | Description | State | Issue
------------ | ------------- | -------------- | ---------------
landing-frame | Frame with a responsive __Toolbar__, __Sidenav__, __Footer__ and __PageContent__ with __Header__ and __Body__ | Done! | -
landing-header | Page header with a image slider, title and text | Done! | -
landing-section | Page section with theme color and content | Doing... | - 

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding
Softness is the conceptSoftness is the concept
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to GitHub Pages

Run `ng github-pages:deploy` to deploy to GitHub Pages.

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
