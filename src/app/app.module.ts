import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { Parallax,
         ParallaxConfig } from 'ng2-parallax/commonjs';
import { PageSliderModule } from 'ng2-page-slider/';
import { MaterializeModule } from 'angular2-materialize';

import { AppComponent } from './app.component';
import { LandingFrameComponent } from './landing-frame/landing-frame.component';
import { LandingHeaderComponent } from './landing-header/landing-header.component';
import { LandingSectionComponent } from './landing-section/landing-section.component';


@NgModule({
  declarations: [
    AppComponent,
    LandingFrameComponent, 
    Parallax, LandingHeaderComponent, LandingSectionComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule, 
    MaterializeModule, 
    PageSliderModule
  ],  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
