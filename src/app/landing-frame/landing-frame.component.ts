import { 
  Component, 
  ElementRef, 
  Renderer, 
  HostListener, 
  Inject, 
  Input, 
  OnInit, 
  trigger,
  state,
  style,
  transition,
  animate, 
  NgZone
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'landing-frame',
  templateUrl: './landing-frame.component.html',
  styleUrls: ['./landing-frame.component.scss'],  
  animations: [
    trigger('sidenavState', [
      state('opened', style({
        left: '0px'
      })),
      state('closed',   style({
        left: '-300px'
      })),
      transition('closed <=> opened', animate('200ms ease-in'))
    ]), 
    trigger('foregroundState', [
      state('visible', style({
        opacity: 0.5, 
        display: 'block'
      })), 
      state('invisible', style({
        opacity: 0.0, 
        display: 'none'
      })), 
      transition('visible <=> invisible', animate('200ms ease-in'))
    ]), 
    trigger('rippleWrapperState', [
      state('visible', style({
        opacity: 0.5, 
        display: 'fixed'
      })), 
      state('invisible', style({
        opacity: 0.0, 
        display: 'none'
      }))
    ]), 
    trigger('rippleState', [
      state('collapsed', style({
        width: '0px', 
        height: '0px', 
        opacity: 1.0
      })), 
      state('expanded', style({
        width: '400vh', 
        height: '400vh',  
        opacity: 0.0, 
        'border-radius': '0px'
      })), 
      transition('collapsed => expanded', animate('800ms ease-in'))
    ]), 
    trigger('fabState', [
      state('visible', style({
        bottom: '26px'
      })), 
      state('invisible', style({
        bottom: '-60px'
      })), 
      transition('visible <=> invisible', animate('200ms ease-in'))
    ])
  ], 
})
export class LandingFrameComponent implements OnInit {

  private toolbarTheme: string = 'transparent';
  private sidenavState: string = 'closed';
  private foregroundState: string = 'invisible';
  private rippleWrapperState: string = 'invisible';
  private rippleState: string = 'collapsed';
  private fabState: string = 'invisible';
  private bodyColor: string = 'blue-grey lighten-5';

  @Input()
  private title: string;
  @Input()
  private logo: string;
  @Input()
  private menuLinks = [];
  @Input()
  private activeLink;
  @Input()
  private actions = [];
  @Input()
  private footer;
  @Input()
  private copyright;

  constructor(
    @Inject(DOCUMENT) private document: Document, 
    private elementRef: ElementRef,
    private renderer: Renderer, 
    public zone: NgZone
  ) {
    
  }

  ngOnInit() {
    
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = this.document.body.scrollTop;
    if (number > 80) {
      this.toolbarTheme = 'dark';
    } else {
      this.toolbarTheme = 'transparent';
    }

    let bodyHeight = this.elementRef.nativeElement.querySelector('.body-wrapper').offsetHeight;
    if(number > (window.screen.height) * 0.8 && number < bodyHeight - 80 /* Toolbar height */) {
      this.fabState = 'visible';
    } else {
      this.fabState = 'invisible';
    }
  }

  toggleSidenav() {
    if(this.sidenavState == 'closed') {
      this.sidenavState = 'opened';
      this.foregroundState = 'visible';
    } else {
      this.sidenavState = 'closed';
      this.foregroundState = 'invisible';
    }
  }

  doRipple() {
    this.rippleWrapperState = 'visible';
    this.rippleState = 'expanded';
  }

  resetRipple() {
    this.rippleState = 'collapsed';
    this.rippleWrapperState = 'invisible';
  }

}
