import { Component, Input, Inject, ElementRef, Renderer, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Parallax,
         ParallaxConfig } from 'ng2-parallax/commonjs';

@Component({
  selector: 'landing-header',
  templateUrl: './landing-header.component.html',
  styleUrls: ['./landing-header.component.scss']
})
export class LandingHeaderComponent implements OnInit {

  @Input()
  private pages = [];
  private pageNumber : number = 0;
  private pageCount : number = 0;
  private parallaxConfig: ParallaxConfig;

  constructor(
    @Inject(DOCUMENT) private document: Document, 
    private elementRef: ElementRef,
    private renderer: Renderer
  ) {
    let screenWidth = this.document.body.offsetWidth;
    if(screenWidth <= 600) {
      this.parallaxConfig = { ratio: 0 };
    } else {
      this.parallaxConfig = { ratio: 1 };
    }
  }

  ngOnInit() {
    this.pageCount = this.pages.length;
  }

  overrideSliderIcon() {
    
  }

}
