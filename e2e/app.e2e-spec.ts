import { SoftUIPage } from './app.po';

describe('soft-ui App', function() {
  let page: SoftUIPage;

  beforeEach(() => {
    page = new SoftUIPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
